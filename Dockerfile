FROM alpine:3 AS hlstats-builder

RUN apk add --no-cache git \
        && git clone https://github.com/NomisCZ/hlstatsx-community-edition.git /hlstats \
        && apk del git

####################
# http
FROM php:7-apache AS hlstats-http

RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli

COPY --from=hlstats-builder /hlstats/web /var/www/html

####################
# deamon
FROM alpine:3 AS hlstats-deamon

ENV GEO_LITE_CITY_API_KEY: "bKYLMNEm1QBHNkgK"

WORKDIR /home/hlstats/

COPY --from=hlstats-builder /hlstats/scripts .

RUN set -x \
        && runDeps=' \
            perl \
            bash \
            su-exec \
            perl-dbd-mysql \
            perl-dbi \
            dcron \
        ' \
        && apk add --no-cache --virtual .build-deps \
            $runDeps \
            wget \
            curl \
            tar \
            ca-certificates \
            make \
            git \
            perl-utils \
            perl-app-cpanminus \
            perl-dev \
            perl-dbi \
            perl-dbd-mysql \
            perl-sub-install \
            perl-namespace-autoclean \
            perl-list-allutils \
            perl-datetime \
            perl-lwp-protocol-https \
            perl-lwp-useragent-determined \
        && cpanm install MaxMind::DB::Types \
        && cpanm install MaxMind::DB::Reader \
        && cpanm install GeoIP2::Database::Reader \
        && addgroup -S hlstats \
        && adduser -S -h /home/hlstats/ -s /bin/bash -g hlstats hlstats \
        && rm -rf /var/cache/apk/* \
        && chmod +x hlstats-awards.pl hlstats.pl hlstats-resolve.pl run_hlstats \
        && echo $'*/5 * * * * cd /home/hlstats/ && su-exec hlstats ./run_hlstats start >/dev/null 2>&1' >> /root/daemon.txt \
        && echo $'15 00 * * * cd /home/hlstats/ && su-exec hlstats ./hlstats-awards.pl >/dev/null 2>&1\n' >> /root/daemon.txt

RUN ls -al \
        && sed -i -e 's/API_KEY="<YOUR_API_KEY>"/API_KEY="${GEO_LITE_CITY_API_KEY}"/g' ./GeoLiteCity/install_binary.sh \
        && chmod +x GeoLiteCity/install_binary.sh \
        && ./GeoLiteCity/install_binary.sh \
        && chown hlstats:hlstats -R . \
	    && apk add --virtual .httpd-rundeps $runDeps \
	    && apk del .build-deps

COPY docker/hlstats-deamon/entrypoint /usr/local/bin/hlstats-deamon-entrypoint
RUN chmod +x /usr/local/bin/hlstats-deamon-entrypoint

EXPOSE 27500/udp

ENTRYPOINT ["/usr/local/bin/hlstats-deamon-entrypoint"]

####################
# cstrike
FROM debian:10-slim AS cstrike

# labels
ARG BUILD_DATE
ARG VCS_REF
LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="https://github.com/febLey/counter-strike_server"

# define default env variables
ENV PORT 27015
ENV MAP de_dust2
ENV MAXPLAYERS 16
ENV SV_LAN 0

# install dependencies
RUN dpkg --add-architecture i386 \
        && apt-get update \
        && apt-get -qqy install lib32gcc1 curl libsdl2-2.0-0:i386 rsync \
        && rm -rf /var/lib/apt/lists/*

WORKDIR /root

# install CS 1.6 via steamcmd with metamod and amxmod
RUN mkdir Steam .steam \
        && cd Steam \
        && curl -sqL "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz" | tar zxvf - \
        && ./steamcmd.sh +login anonymous +force_install_dir /hlds +app_update 90 validate +quit || true \
        && ./steamcmd.sh +login anonymous +app_update 70 validate +quit || true \
        && ./steamcmd.sh +login anonymous +app_update 10 validate +quit || true \
        && ./steamcmd.sh +login anonymous +force_install_dir /hlds +app_update 90 validate +quit \
        && cd /root/.steam \
        && ln -s ../Steam/linux32 sdk32 \

        # install metamod
        && mkdir -p /hlds/cstrike/addons/metamod/dlls \
        && cd /hlds/cstrike/addons/metamod/dlls \
        && curl https://vorboss.dl.sourceforge.net/project/metamod/Metamod%20Binaries/1.20/metamod-1.20-linux.tar.gz --output metamod-1.20-linux.tar.gz \
        && tar -xzvf metamod-1.20-linux.tar.gz \
        && rm metamod-1.20-linux.tar.gz \
        && sed -i 's/dlls\/cs.so/addons\/metamod\/dlls\/metamod_i386.so/g' ../../../liblist.gam \

        # install amxmod
        && cd /hlds/cstrike \
        && curl https://www.amxmodx.org/release/amxmodx-1.8.2-base-linux.tar.gz --output amxmodx-1.8.2-base-linux.tar.gz \
        && tar -xzvf amxmodx-1.8.2-base-linux.tar.gz \
        && rm amxmodx-1.8.2-base-linux.tar.gz \
        && echo 'linux addons/amxmodx/dlls/amxmodx_mm_i386.so' >> addons/metamod/plugins.ini

COPY docker/cstrike/entrypoint /usr/local/bin/docker-cstrike-entrypoint
RUN chmod +x /usr/local/bin/docker-cstrike-entrypoint
RUN mkdir /custom-maps

# start server
WORKDIR /hlds
ENTRYPOINT ["/usr/local/bin/docker-cstrike-entrypoint"]

####################
# cstrike-custom-maps
FROM alpine AS cstrike-custom-maps
WORKDIR /custom-maps

RUN mkdir cstrike \
    && cd cstrike \
    && mkdir maps \
    && mkdir sound \
    && mkdir models \
    && mkdir overviews

RUN apk add --no-cache --virtual .build-deps unzip curl unrar rsync \
        && rm -rf /var/lib/apt/lists/*

# aim_map
RUN curl -L https://gamebanana.com/dl/320784 --output aim_map.zip \
        && unzip aim_map.zip \
        && rm aim_map.zip \
        && mv aim_map.bsp cstrike/maps

# aim_headshot_2020
RUN curl -L https://gamebanana.com/dl/488435 --output aim_headshot_2020.zip \
        && unzip aim_headshot_2020.zip \
        && rm aim_headshot_2020.zip read--me.txt

# aim_crazyjump_2
RUN curl -L https://gamebanana.com/dl/318432 --output aim_crazyjump_2.zip \
        && unzip aim_crazyjump_2.zip \
        && rm aim_crazyjump_2.zip \
        && mv aim_crazyjump* cstrike/maps

# fy_pool_day_2
RUN curl -L https://gamebanana.com/dl/317221 --output fy_pool_day_2.rar \
        && unrar e fy_pool_day_2.rar \
        && rm fy_pool_day_2.rar \
        && mv fy_pool_day* cstrike/maps

# rats_map_pack
RUN curl -L https://gamebanana.com/dl/290437 --output rats_map_pack.rar \
        && unrar e rats_map_pack.rar \
        && rm rats_map_pack.rar Instructions.txt \
        && mv cs_rats*.* cstrike/maps \
        && mv de_rats*.* cstrike/maps

#ENTRYPOINT ["rsync -r --progress /custom-maps/* /maps-sync"]

CMD ["rsync", "-r", "--progress", "/custom-maps/", "/maps-sync"]
